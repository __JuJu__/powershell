function Get-DfirQueryRecursively
{
    [CmdletBinding()]
    param(
    [Parameter()]
    [String]$PATH_TO_QUERY='C:\Users\juju\Desktop\',
    [Parameter()]
    [String]$TARGET='Security.evtx',
    [Parameter()]
    [String]$QUERRY="SELECT COUNT(*) AS CNT, EventID FROM 'Security.evtx' GROUP BY EventID ORDER BY CNT DESC"
    )
    <#
    .Synopsis
    Apply a querry to the log files under the selected directory.

    .Description
    Displays a visual representation of a calendar. This function supports multiple months
    and lets you highlight specific date ranges or days.

    .Parameter FirstDayOfWeek
    The day of the month on which the week begins.

    .Parameter HighlightDay
    Specific days (numbered) to highlight. Used for date ranges like (25..31).
    Date ranges are specified by the Windows PowerShell range syntax. These dates are
    enclosed in square brackets.

    .Parameter HighlightDate
    Specific days (named) to highlight. These dates are surrounded by asterisks.

    .Example
    # Show a default display of this month.
    Show-Calendar

    .Example
    # Display a date range.
    Show-Calendar -Start "March, 2010" -End "May, 2010"

    .Example
    # Highlight a range of days.
    Show-Calendar -HighlightDay (1..10 + 22) -HighlightDate "December 25, 2008"
    #>
    $SQL='C:\Program Files (x86)\Log Parser 2.2\LogParser.exe'
    get-ChildItem -Path $PATH_TO_QUERY -Recurse| Where-Object {$_.Name -eq $TARGET} | ForEach-Object { Set-Location $_.directoryName; Get-Location; & $SQL -stats:OFF -i:EVT $QUERRY; Set-Location ..; }
}