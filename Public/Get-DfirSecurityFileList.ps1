function Get-DfirSecurityFileList
{
    [CmdletBinding()]
    param($PATH_TO_ZIP=".",
    $PATH_NEW_FOLDERS='C:\Users\juju\Desktop\',
    $TARGET='Security.evtx') 

    [void][Reflection.Assembly]::LoadWithPartialName('System.IO.Compression.FileSystem')
    $ZIP_TO_BE_INVESTIGATED = Get-ChildItem -Recurse -File -Filter "*.zip" -Path $PATH_TO_ZIP
   foreach($sourceFile in $ZIP_TO_BE_INVESTIGATED)
    {
        [IO.Compression.ZipFile]::OpenRead($sourceFile.FullName).Entries | ForEach-Object {if($_.FullName -match "/$TARGET"){
            $path_folder_name = Join-Path -Path $PATH_NEW_FOLDERS -ChildPath $sourceFile
            $path_folder_name = $path_folder_name.split('.')[0]
            if(-not (Test-Path -Path $path_folder_name))
                {
                    new-item -ItemType "directory"-path $path_folder_name
                    $FileName = $_.Name
                    [System.IO.Compression.ZipFileExtensions]::ExtractToFile($_, "$path_folder_name\$FileName", $true)
                }
            }
        }
    }
}